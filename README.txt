SCALD FILE ALIAS
---------------------------------

Homepage: http://drupal.org/project/scald_file_alias

Contents of this file:

 * Introduction
 * Installation

INTRODUCTION
------------

This module adds alias support for scald files.  It also overrides the default 'full' display for scald file atoms to present instead a downloadable interface.

INSTALLATION
------------

Scald file alias depends on the following module:

- Scald version 1.2 or higher
- Scald file


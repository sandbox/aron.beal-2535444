<?php
/**
 * @file
 *   Override of default theme implementation for Scald File Render.
 */
?>
<img src="<?php print file_create_url($vars['thumbnail_source']); ?>" class="scald-file-icon" alt="file type icon" />
<a href="<?php print file_create_url($vars['path']); ?>" title="<?php print $vars['file_title']; ?>">
  <?php print $vars['file_title']; ?>
</a>

<?php
/**
 * @file
 * Adds filesize field to views
 */

/**
 * Implements hook_views_data().
 * Provides an additional handler for views to
 */
function scald_alias_views_data() {
  $data = array();
  $data['scald_atoms']['alias'] = array(
    'title' => t('URL Alias'),
    'real field' => 'sid', //irrelevant
    'help' => t("Scald Atom alias, if set."),
    'field' => array(
      'handler' => 'scald_alias_views_handler_field_alias',
      'click sortable' => TRUE,
    ),
  );
  return $data;
}
